package com.bruno.wssoap.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.bruno.wssoap.calculator.Calculator;
import com.bruno.wssoap.calculator.GetCalculatorRequest;
import com.bruno.wssoap.calculator.GetCalculatorResponse;
import com.bruno.wssoap.repositories.CountryRepository;

@Endpoint
public class CountryEndpoint {
	private static final String NAMESPACE_URI = "http://bruno.com/wssoap/calculator";

	private CountryRepository countryRepository;

	@Autowired
	public CountryEndpoint(CountryRepository countryRepository) {
		this.countryRepository = countryRepository;
	}

//	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getCalculatorRequest")
//	@ResponsePayload
//	public GetCalculatorResponse getCountry(@RequestPayload GetCalculatorRequest request) {
//		GetCalculatorResponse response = new GetCalculatorResponse();
//		Calculator calculator = new Calculator();
//		calculator.setResult(countryRepository.add(request.getA(), request.getB()));
//		response.setResult(calculator);
//		return response;
//	}
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getCalculatorRequest")
	@ResponsePayload
	public GetCalculatorResponse subtract(@RequestPayload GetCalculatorRequest request) {
		GetCalculatorResponse response = new GetCalculatorResponse();
		Calculator calculator = new Calculator();
		calculator.setResult(countryRepository.subtract(request.getA(), request.getB()));
		response.setResult(calculator);
		return response;
	}
}