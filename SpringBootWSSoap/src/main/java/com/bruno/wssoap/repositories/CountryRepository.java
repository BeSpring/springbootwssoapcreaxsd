package com.bruno.wssoap.repositories;

import org.springframework.stereotype.Component;

@Component
public class CountryRepository {
	public int add(int a, int b) {
		return a + b;
	}

	public int subtract(int a, int b) {
		int result;
		if (a > b)
			result = a - b;
		else
			result = b - a;
		return result;
	}

}
